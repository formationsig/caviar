## Modélisation et description de la structure
### L’opération archéologique  : de la prescription à la carte archéologique en passant par ~~la Lorraine~~ l’opérateur

Le cadre géographique, administratif et scientifique de référence communément appelé “opération archéologique” recouvre plusieurs réalités selon la logique qui prévaut à un moment T du processus. Globalement, tout le monde sait ce qu’est une opération archéologique. Dans le détail, une opération archéologique recouvre plusieurs réalités. La modélisation de l’opération archéologique dans le cadre de l’élaboration du catalogue nécessite de traduire en entité graphique des concepts administratifs qui n’avaient pas nécessairement de matérialité géographique jusque là.

L’emprise prescrite détermine le cadre administratif d’intervention. Par convention, dans le catalogue, on donne, à l’emprise, le numéro de l’arrêté qui la définit. Dans le cas le plus fréquent et le plus simple, à une emprise prescrite côté SRA, correspond, côté opérateur, une emprise décapée (fouille) ou une emprise délimitant l’espace échantillonné (diagnostic). C’est la matérialisation de l’opération archéologique au sens de la carte archéologique nationale (OA) :

>1 prescription = 1 opération Inrap (numope) = 1 opération avec identifiant national

Cependant, il arrive aussi qu’une emprise de prescription fasse l’objet d’une subdivision motivée par les contraintes pratiques ou scientifiques d’intervention. Dans ce cas, deux cas de figure se présentent : soit l’emprise prescrite donne lieu à la réalisation de deux chantiers distincts ayant chacun le même numéro de prescription mais pas le même numéro d’opération, sur le modèle :  

>1 prescription = n opérations Inrap (numope) = n Opération Archéologiques

soit, par fidélité aux services prescripteurs, on conserve un numéro d’opération unique mais on procède à un découpage de l’emprise prescrite en tranches (à la demande ou non du SRA) :

>1 prescription = 1 opération Inrap (numope) = n tranches = 1 Opération Archéologiques (autant d’OA qu’il y a de tranches Inrap?)

Dans ces deux derniers cas, qu’elle soit dénommée “opération” ou “tranche”, l’emprise géographique de l’opération correspond à une subdivision de l’emprise prescrite d’origine.

Pour pallier la diversité des pratiques en la matière et conserver un modèle de numérotation identique d’une opération à l’autre, l’unité de référence retenue pour le catalogue correspond à la plus petite subdivision existante, soit la tranche (on ajoutera par défaut une tranche “1” à chaque opération).

Ainsi, dans le catalogue, une opération archéologique est définie par une emprise géographique d’un seul tenant ou non (polygone multipartie), un identifiant unique, un responsable d’opération, une date de début/fin d’opération. Selon les cas, l’emprise géographique de l’opération correspondra strictement ou partiellement à l’emprise initialement prescrite (cf table Emprise).

### Modèle conceptuel de données

Le catalogue est composé de trois tables de données principales :

- la table emprise contient toutes les surfaces prescrites ainsi que les portions de surface prescrite ayant donné lieu à une opération Inrap (cf développement ci-dessus) ; c’est le cadre de référence pour la rédaction du rapport et plus généralement, l’accès aux données produites dans le cadre de l’opération ;

- la table ouverture est destinée à stocker toute les unités techniques réalisées à des fins d’observation du sous-sol ayant fait l’objet d’un lever topographique sur le terrain et représentées dans l’espace par un polygone ;

- la table uniteobservation contenant toutes les unités archéologiques ayant fait l’objet d’un lever topographique sur le terrain et représentées dans l’espace par un polygone ou un point (cette table résulte de l’association des tables poly et point à l’échelle de l’opération (cf guide_6_couches).

![Modèle conceptuel de données](img_caviar1/01_MCD.png)

### Schéma conceptuel de données

![Schéma conceptuel de données selon la méthode HBDS](img_caviar1/02_SCD_HBDS.png)

Schéma conceptuel de données selon la méthode HBDS

<img src="img_caviar1/03_SCD_simple.jpg"  width="500">

Schéma conceptuel de données simplifié

### Description des tables

#### Général

Chaque table contient un numéro unique d’incrémentation (*gid*), le type de géométrie (*geom*) et la clé étrangère de la table de référence, soit l’identifiant (*gidoperef*) de l’emprise.

#### Table emprise

La table **emprise** contient les emprises de prescription telles que décrites et représentées dans les arrêtés.
Le tableau ci-dessous fait état des principaux champs constitutifs de la table :  
- en vert, les champs directement issus du SGA ;
- en rosé, les champs alimentés par Dolia ;
- en gris, les champs remplis par le producteur de la donnée (cf guide_6_couches) ;
- les champs sans couleur de remplissage sont complétés automatiquement par le serveur.

nom de champ|description|type champ|source|type de saisie|liste valeurs
:--|:--|:--|:--|:--|:--
gid|id unique, clé primaire|integer|caviar|automatique|none
surface|surface de l'emprise réelle|numeric(9,2)|caviar|automatique|none
annee_terrain|année début terrain|integer|SGA|interface|none
ro|arrêté de désignation du ro|varchar(50)|SGA|interface|liste SGA
numope|numéro SGA|varchar(12)|id. opération|manuelle|none
typope|fouille, diag, étude...|varchar(25)|SGA|interface|liste SGA
typemp|prescrite ou topographiée|varchar(30)|caviar|manuelle|liste fermée
nomope|nom usuel|varchar(50)|SGA|interface|none
numoa|numéro OA|varchar(20)|SGA|interface|none
numprescr|numéro du dernier arrêté de prescription|varchar(25)|SGA|interface|none
tranche|numéro de la tranche (1 par défaut)|varchar(2)|id. opération|manuelle|none
code_tranche|concaténation du code opération et du numéro de tranche|varchar(15)|id. opération|automatique|none
periode|indication des différentes périodes chronologiques identifiées à partir des datations de structures|varchar|caviar|automatique|liste fermée
notice_rapport|lien ark vers le rapport et la notice dans Dolia|hyperlien|dolia|automatique|none
geom|géométrie des entités de la couche|multipolygon, 2154|caviar|automatique|liste fermée, valeur unique
...|...|...|...|...|...

Pour optimiser la saisie des données, la DSI a développé une interface permettant de relier le catalogue au SGA et à Dolia (cf § Paramétrage de la connexion à Dolia). Cette interface permet, par l’intermédiaire de mises à jour régulières, d’afficher dans le catalogue des données saisies dans les autres systèmes d’informations de l’Inrap garantissant ainsi la cohérence et la complétude des informations d’un système à l’autre. C’est le cas notamment pour les données descriptives d’une opération.

#### Principaux champs de métadonnées associées (table **emprise**)

- en jaune : les champs saisis par l’intégrateur (saisis, à terme par le producteur de la donnée?)

nom de champ|description|type champ|source|type de saisie|liste valeurs
:--|:--|:--|:--|:--|:--
publisher|nom de l'organisme qui produit la donnée, à développer, pas d'acronyme|character varying(80)|caviar|automatique|fermée
description|décrit l'historique du jeu de données pour la table "Emprise", "ouverture" et "unobs"|character varying(250)|caviar|manuelle|liste ouverte
resolution_emprise|décrit le niveau de détail de la ressource, pour les emprises|character varying|caviar|manuelle|liste ouverte
resolution_ouv_unobs|décrit le niveau de détail de la ressource, pour les ouvertures et les unités d'observation|character varying|caviar|manuelle|liste ouverte

Une métadonnée est une donnée permettant de décrire et caractériser une donnée ou une ressource documentaire (une donnée sur la donnée).  Les métadonnées facilitent la gestion interne des ressources (au sens large) et permettent d’optimiser la recherche et la localisation des données et des documents. Il existe plusieurs normes internationales définissant les métadonnées par secteur d’activité.

Dans le cas présent, pour simplifier au maximum la saisie des métadonnées, nous avons choisi de les associer à l’emprise uniquement - et non à chaque entité représentée du catalogue - partant du postulat que les données de terrain sont acquises de manière uniforme au sein d’une même opération. La directive européenne Inspire et la norme internationale DUBLIN CORE (ISO 15836) ont dicté le choix des champs de métadonnées constitutifs de la table emprise du catalogue.

Dans ce document, seuls 4 des champs de métadonnées renseignés sont présentés.

Pour plus d’information, consulter le guide dédié (guide_metadonnees).

Pour faciliter la consultation des données d’emprise pour les utilisateurs de ce guide, une vue **emprise_agent** a été créée (cf. paragraphe Les vues utilisateurs). C’est cette couche qu’il faudra afficher dans le client -SIG.

#### Table **ouverture**

La table **ouverture** contient toutes les unités techniques réalisées à des fins d’observation du sous-sol et représentées dans l’espace par un objet surfacique. En d’autres termes, cette table contient les portions de sol réellement investiguées par les archéologues.

Elle est constituée de 5 champs :
- en gris, les champs remplis par le producteur de la donnée (cf guide_6_couches);
- les champs sans couleur de remplissage sont complétés automatiquement par le serveur.

nom de champ|description|type champ|source|type de saisie|liste valeurs
:--|:--|:--|:--|:--|:--
gid|id unique, clé primaire|integer|caviar|automatique|none
numouvert|numéro d'enregistrement terrain|varchar|terrain|manuelle|none
typouvert|typologie des ouvertures|varchar|terrain|manuelle|liste fermée*
geom|géométrie des entités de la couche|polygon|caviar|automatique|liste fermée
gidoperef|clé étrangère référence emprise|integer|caviar|automatique|none

Le champ *numouvert* renvoie au numéro d’enregistrement de l’unité soit le numéro attribué par le responsable d’opération. Ce numéro permet de joindre les données spatiales avec les données descriptives du système d’enregistrement de terrain ou de l’inventaire (cf guide_6_couches).

Le champ *typouvert* renvoie à la typologie des ouvertures (liste de valeurs fermée) :
- “tranchée” : unité technique principale utilisée en diagnostics
- “sondage” : ouverture bien circonscrite réalisée dans une tranchée (diagnostic) ou dans l’espace décapé (fouille)
- “sondage profond” : sondage à des fins d’observations géologiques, de recherche de niveaux paléo etc.
- “fenêtre” : extension limitée de la tranchée réalisée pour suivre les vestiges apparus dans la tranchée principale
- “décapage” : dans le cas d’une fouille, le décapage équivaut à l’emprise réellement décapée soit l’emprise prescrite moins les surfaces inaccessibles ou non investiguées pour raisons pratiques ou méthodologiques ; dans le cas où la zone décapée correspond strictement aux contours de l’emprise prescrite, l’entité est enregistrée à la fois dans la table emprise en tant qu’emprise prescrite et dans la table ouverture en tant que décapage
- “palier” : zone(s) successive(s) destinée(s) à respecter les normes de sécurité lors d’une fouille en profondeur

#### Table **uniteobservation**

La table **uniteobservation** contient toutes les unités d’observation archéologiques relevées sur le terrain et représentées sous forme d’objets surfaciques et/ou de points. Elle résulte de l’association des tables **NumSGA_poly** et **NumSGA_point** à l’échelle de l’opération (cf guide_6_couches).

- En gris, les champs remplis par le producteur de la donnée (cf guide_6_couches)

nom de champ|description|type champ|source|type de saisie|liste valeurs
:--|:--|:--|:--|:--|:--
gid|id unique|integer|caviar|automatique|none
numunobs|numéro d'enregistrement terrain|integer|terrain|manuelle|none
typunobs|typologie des unités d'enregistrement archéologiques (US, fait, isolat)|varchar(20)|terrain|manuelle|liste fermée
interpret|interprétation fonctionnelle des vestiges|varchar(50)|terrain|manuelle|liste ouverte
datedebut|borne inférieure ou “TPQ “|bigint|terrain|manuelle|none
datefin|borne supérieure ou “TAQ"|smallint|terrain|manuelle|none
gidoperef|clé étrangère référence emprise|integer|caviar|automatique|none
periode|période générique|varchar(50)|caviar|automatique****|liste fermée
geom|géométrie des entités de la couche|(multipolygon, 2154)(point,2154)|caviar|automatique|liste fermée valeur unique

Le champ *numunobs* renvoie au numéro d’enregistrement de l’unité d’enregistrement soit le numéro attribué par le responsable d’opération (entier recommandé). Ce numéro permet de joindre les données spatiales de la table **uniteobservation** avec les données descriptives enregistrées dans le système d’enregistrement de terrain ou l’inventaire de l’opération (cf guide_6_couches).
Le champ *typunobs* renvoie à la typologie des unités d'enregistrement archéologiques : US, fait, isolat. Il permet notamment d’interroger les unités en fonction de leur géométrie (polygones ou points).

Le champ *interpret* correspond à l’interprétation fonctionnelle des vestiges observés. Les valeurs saisies correspondent strictement aux valeurs figurant sur les inventaires contenus dans le rapport. Ce champ n’a pas fait l’objet d’une harmonisation nationale, il n’est donc pas utilisable en l’état pour requêter à l’échelle de plusieurs opérations. A l’avenir, l’utilisation d’un vocabulaire contrôlé dans un champ propre à Caviar n’est pas exclue.

Les champs *datedebut*, *datefin* et *periode* sont les trois champs chronologiques du catalogue. Ils font l’objet d’un développement propre ci-dessous.

#### Les champs chronologiques : datedebut, datefin et le champ periode de CAVIAR

Les valeurs des champs *datedebut* et *datefin* de la couche **uniteobservation** de CAVIAR sont directement issues de celles des fichiers de formes **numSGA_poly** et **numSGA_point** de chaque opération, soit des champs *datedebut* et *datefin* de ces deux couches quand l’information existe (cf guide_6_couches).
Les valeurs numériques des champs *datedebut* et *datefin* entraînent la création automatique dans le catalogue d’une occurrence en texte dans le champ *periode* à partir du tableau de conversion ci-dessous :

datedebut|datefin|periode
:--|:--|:--
\>=1789||contemporain
\>= 1492|<= 1789|moderne
\>= 999|<= 1492|médiéval classique
\>= 750|<= 999|carolingien
\>= 476|<= 750|mérovingien
\>= 476|=1492|médiéval
\>= -50|<= 476|antique
\>= -800| <= -50|âge du Fer
\>= -2200|<= -800|âge du Bronze
\>= -2200|<= -50|protohistoire
\>= -5500|<= -2200|néolithique
\>= -10000|<= -5500|mésolithique
\>= -40000|<= -10000|paléolithique supérieur
\>= -300000|<= -40000|paléolithique moyen
\>= -800000|<= -300000|paléolithique inférieur
\>= -800000| <= -10000|paléolithique
_|_|indéterminé
sinon|_|multiple

Le tableau d’équivalences est adapté à une large partie du territoire métropolitain. Il tient compte des spécificités régionales (période sarrasine, wisigothique, etc) lorsque les données ont été transmises. Ainsi, les valeurs de *datedebut* et de *datefin* correspondant à la période antique peuvent varier selon les régions. Il est possible de faire évoluer le tableau d’équivalences.
Le découpage en grandes périodes reste volontairement large pour éviter toute méprise liée à l’affectation d’une dénomination arbitraire à une fourchette chronologique précise. Le champ *periode* est donc affiché à titre indicatif. Pour toute requête complexe impliquant une périodisation plus fine, privilégier les champs *datedebut* et *datefin*.

#### Exemples de requêtes en sql

 - sélection des entités antiques pour une opération donnée :
	```sql
	SELECT * 	
	FROM activite.uniteobservation
	WHERE periode LIKE 'antique' AND gidoperef = '123456'
	```
	gidoperef est la clef primaire gid de l’opération concernée.

- sélection des entités médiévales sur une commune :
	```sql
	SELECT u.*
	FROM activite.uniteobservation u, activite.communes com
	WHERE st_intersects(u.geom, com.geom) AND com.nom_com LIKE 'ma_commune' AND periode LIKE 'medieval'
	```
- sélection des entités postérieures à la chute de l’empire romain d’Occident et antérieures à l’an 1000 :
	```sql
	SELECT  *
	FROM activite.uniteobservation
	WHERE datedebut > 476 AND datefin < 1000
	```
## La vue emprise_agent

Les vues sont créées à partir de requêtes sur une à plusieurs tables. Totalement transparentes pour l’utilisateur, elles permettent d’afficher tout ou partie des champs d’une ou plusieurs tables dans une même fenêtre. Elles sont gérées/manipulées par l’utilisateur depuis son poste client comme n’importe-quelle couche de données. Pour faciliter la consultation du catalogue, plusieurs vues ont été paramétrées à partir des tables précédemment décrites, par profil d’utilisateur. Pour les utilisateurs du présent guide, la vue dédié est dénommé **emprise_agent** : elle propose un résumé de la table **emprise** avec l’affichage des champs permettant d’identifier l’opération ainsi qu’une sélection des champs de métadonnées susceptibles d’intéresser les utilisateurs (cf. guide_metadonnees ).

nom de champ|description|type champ|source|type de saisie|liste valeurs
:--|:--|:--|:--|:--|:--|
gid|id unique, clé primaire|integer|caviar|automatique|none
surface|surface de l'emprise réelle|numeric(9,2)|caviar|automatique|none
annee_terrain|année début terrain|integer|SGA|interface|none
ro|arrêté de désignation du ro|varchar(50)|SGA|interface|liste SGA
numope|numéro SGA|varchar(12)|id. opération|manuelle|none
typope|fouille, diag, étude...|varchar(25)|SGA|interface|liste SGA
typemp|prescrite (ou topographiée pour les opération anciennes)|varchar(30)|caviar|manuelle|liste fermée*
nomope|nom usuel|varchar(50)|SGA|interface|none
numoa|numéro OA|varchar(20)|SGA|interface|none
numprescr|numéro du dernier arrêté de prescription|varchar(25)|SGA|interface|none
annee_prescription|1ère Année Emission prescription|integer|SGA|automatique depuis SGA|none
tranche|numéro de la tranche (1 par défaut)|varchar(2)|id. opération|manuelle|none
code_tranche|concaténation du code opération et du numéro de tranche|varchar(15)|id. opération|automatique|none
notice_rapport|lien ark vers le rapport et la notice dans Dolia|hyperlien|dolia|automatique|none
geom|géométrie des entités de la couche|multipolygon, 2154|caviar|automatique|liste fermée, valeur unique
publisher|nom de l'organisme qui produit la donnée|character varying(80)|caviar|automatique|fermée
description|décrit l'historique du jeu de données pour les tables **emprise**, **ouverture** et **unobs**|character varying(250)|caviar|manuelle|liste ouverte*

Pour la description des champs, se reporter au paragraphe “table Emprise”.

## Connexion au catalogue depuis QGIS

### Paramétrer la connexion (uniquement la première fois)

1. ouvrir QGIS
2. ajouter une couche PostGIS  
 ![Ajouter une couche PostGIS 1](img_caviar1/04_QGIS1.jpg)
 
3. cliquer sur "Nouveau"  
![Nouvelle couche PostGIS](img_caviar1/05_QGIS2.png)

4. paramétrer la connexion  
![Paramétrer la connexion](img_caviar1/06_QGIS3.png)

	- Base de données = activite
	- Nom utilisateur = agent
	- Mot de Passe = agent
5. Enregistrer les nom utilisateur et mot de passe puis cliquer sur ok : la connexion est paramétrée.

### Connexion au catalogue

1. ouvrir QGIS
2. ajouter une couche PostGIS  
 ![Ajouter une couche PostGIS 1](img_caviar1/04_QGIS1.jpg)
 
3. cliquer sur "Connecter"  
 ![Ajouter une couche PostGIS 2](img_caviar1/08_QGIS5.png)
 
4. sélectionner les couches à afficher dans QGIS en cliquant dessus ; les couches qui nous intéressent sont celles dont le type spatial est défini (MultiPolygon et Point), soit différent des couches pour lesquelles il est indiqué « Sélectionner ».  
 ![Sélectionner les couches à afficher](img_caviar1/09_QGIS6.png)  
 Lorsque un point d’exclamation figure au début d’une ligne, il est nécessaire de sélectionner le paramètre clef primaire dans la colonne *id* de l’entité comme sur l’image ci-dessus ; il suffit de cliquer sur la ligne concernée, dans la colonne *id de l’entité* ; sélectionner le champ *id* s'il existe, sinon le champ *gid*
5. cliquer sur "Ajouter" puis Fermer la fenêtre
6. les couches doivent être affichées dans la fenêtre de visualisation de QGIS

## Connexion au catalogue depuis ArcGIS

A partir de d’ArcMap : il faut créer une couche de requête qui va permettre d’accéder au couches de Caviar en mode lecture seule.

### Création de la couche de requête  
 ![Création de la couche de requête 1](img_caviar1/10_ArcGIS1.png)
 
1. Dans la fenêtre qui s’ouvre, cliquer sur connexions puis nouveau  
 ![Création de la couche de requête 2](img_caviar1/11_ArcGIS2.png)
 
2. Saisir les paramètres suivants dans la fenêtre connexion à la base de donnée activite.  
 ![Création de la couche de requête 3](img_caviar1/12_ArcGIS3.png)  
De haut en bas :
	- le logiciel qui héberge la base de donnée : PostgreSQL ;
	- l’adresse IP du serveur 10.210.1.32 ;
	- le nom d’utilisateur agent qui permet d’accéder aux couches de caviar en lecture seule ;
	- le mot de passe : agent ;
	- et le nom de la base de donnée : activite.
3. Appuyer sur OK.  
 ![Création de la couche de requête 4](img_caviar1/13_ArcGIS4.png)

### Accéder aux couches de Caviar

1. Dans la fenêtre nouvelle couche de requête, sélectionner la connexion nouvellement créée  
 ![Accéder aux couches de Caviar 1](img_caviar1/14_ArcGIS5.png)  
La liste des couches s’affiche automatiquement dans l’encadré en haut à gauche.
2. Cliquer sur la couche désirée, la liste des champs apparaît  dans l’encadré en haut à droite.
3. Double-cliquer sur la couche désirée permet l’apparition d’une requête dans l’encadré requête en bas de la fenêtre ; cette requête permet l’affichage de données.  
 ![Accéder aux couches de Caviar 2](img_caviar1/15_ArcGIS6.png)
 
4. Remplir le champ Nom avec un ou des termes dont la seule utilité est pour l’utilisateur d’identifier facilement la couche par la suite sous ArcMap ;
5. Cliquer sur "Valider" en bas à droite pour vérifier la requête ;
6. Cocher “Afficher les options avancées” ;
7. Cliquer sur "Suivant" ;
8. Il convient alors de spécifier au logiciel la clef primaire de la couche : tout décocher sauf gid ; Remarque : si dans le choix de la couche à afficher il existe un champ id et gid, choisir le champ id.  
 ![Accéder aux couches de Caviar 3](img_caviar1/16_ArcGIS7.png)
 
9. Cliquer sur "Terminer".

### Afficher les couches dans ArcMap

Un message d’avertissement ou d’erreur peut survenir. Ignorez-le. Par exemple :  
 ![Afficher les couches de Caviar 1](img_caviar1/17_ArcGIS8.png)
 
1. Ouvrir la table attributaire ;
2. Sélectionner et zoomer sur une emprise ;
3. Dézoomer pour naviguer dans le jeu de données à votre convenance.  
 ![Afficher les couches de Caviar 2](img_caviar1/18_ArcGIS9.png)

## Paramétrage de la connexion à Dolia (depuis la couche **emprise_agent**)

1. Afficher la couche **emprise_agent** comme décrit plus haut
2. ouvrir les propriétés de la couche (clic droit, propriétés ou double-clic sur la couche)
3. cliquer sur "Action" dans la barre de gauche  
 ![Paramétrer Dolia 1](img_caviar1/19_Dolia1.png)
 
4. Ajouter une nouvelle action en cliquant sur le  ![Plus](img_caviar1/00_IconePlus.png) et saisir les informations selon l’exemple ci-dessous (le choix de la description et du nom court sont laissés à l’appréciation de l’utilisateur)  
 ![Paramétrer Dolia 2](img_caviar1/20_Dolia2.png)
 
5. cliquer sur l’epsilon ![Epsilon](img_caviar1/00_IconeEpsilon.png) en bas à droite  pour ouvrir le calculateur d’expression
6. depuis la boîte en haut à droite, dérouler l’onglet “champs et valeurs”et cliquer sur le champ ![Notice](img_caviar1/00_IconeNotice.png) ; l’expression apparaît en vert dans la fenêtre principale de gauche  
 ![Paramétrer Dolia 3](img_caviar1/21_Dolia3.png)
 
7. cliquer sur "OK" pour fermer la fenêtre et revenir à la fenêtre précédente
8. cliquer sur "Insérer" : l’expression apparaît dans l’espace “texte de l’action”  
 ![Paramétrer Dolia 1](img_caviar1/22_Dolia4.png)
 
9. ok  
 ![Paramétrer Dolia 1](img_caviar1/23_Dolia5.png)
 
11. l’action a bien été enregistrée
12. Appliquer, ok pour fermer la fenêtre
13. dans la fenêtre de QGIS, l’icône Action ![Action](img_caviar1/00_IconeAction.png) est devenue active
14. sélectionner l’icône - le curseur prend la forme d’une croix - sélectionner l’action et cliquer sur une emprise pour ouvrir la notice dans Dolia
15. pour accéder au rapport, il est nécessaire d’avoir ouvert une session dans Dolia au préalable depuis l’accès authentifié

## Rechercher une opération
1. Pour rechercher une opération à partir d’un champ d’attribut (numéro de l’opération, nom du responsable d’opération), ouvrir la table attributaire de la couche **emprise_agent**  
 ![Recherche 1](img_caviar1/24_Recherche1.jpg)
2. utiliser le filtre de champ et sélectionner le champ sur lequel va porter la requête comme sur la copie d’écran ci-dessus
3. saisir la valeur rechercher dans la barre de saisie ; pour rechercher par responsable d’opération, il n’est pas nécessaire de respecter la casse ni de saisir le nom et le prénom*  
 ![Recherche 2](img_caviar1/25_Recherche2.png)
4. appuyer sur Entrée pour lancer la recherche
5. pour une requête multicritère, replongez-vous dans les supports de la formation de niveau 1 ou demandez à votre référent SIG préféré.

## Manipulation des données

Les données du catalogue - tables et/ou vues- sont manipulables comme n’importe-quelle autre couches de données. Une fois affichées dans QGIS, vous pouvez modifier le style, étiqueter, requêter, copier, coller et télécharger les données pour travailler “en local” à partir d’une sélection par exemple.
