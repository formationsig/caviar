# Guide pratique d’utilisation du CAtalogue de Visualisation de l’Information ARchéologique (CAVIAR)

Mathias Cunault, Bertrand Houdusse, Anne Moreau, Véronique Vaillé  
février 2019

## Avant-propos

Le catalogue a pour objectif de faciliter la recherche de données géoréférencées produites par l’institut. Ce Système d’Information Géographique n’a pas vocation à répondre à une problématique scientifique ; c’est un inventaire. Il se constitue peu à peu par l’accumulation des données spatiales collectées sur le terrain (diagnostics et fouilles) par les archéologues de l’institut. Le catalogue est le prolongement du déploiement des SIG à l’échelle de l’opération auquel il est fait fréquemment référence dans ce document par le renvoi au “guide des 6 couches”.

Ce catalogue représente autant une forme d’archivage qu’une plateforme d’échange de données susceptibles d’être réutilisées à court ou moyen terme dans les domaines scientifiques et administratifs. Il est conçu et développé pour faciliter la recherche de données, préalable nécessaire à la préparation des opérations et aux projets de recherche. Le catalogue fait partie d’un système plus complexe destiné à rendre accessible l'ensemble des données et de la documentation produites dans le cadre d'une opération.

Hébergé sur l'un des serveurs du siège, le catalogue est géré avec PostGIS, la cartouche spatiale de PostGreSQL. Il est accessible à tous les agents de l'Inrap depuis n'importe-quel poste informatique via un client-SIG de type QGIS ou ArcGIS.

Ce guide décrit la structure du catalogue, les couches de données mises à disposition  ainsi que la procédure de connexion ; il s’adresse aux archéologues de l’Institut.

Pour toute question relative au serveur et au catalogue de données spatiales, s’adresser à :  
anne.moreau[at]inrap.fr  
mathias.cunault[at]inrap.fr  